import boto3
import logging
import os

from datetime import timedelta, datetime
from dateutil import parser

# Setup logging for function
logger = logging.getLogger()
logger.setLevel(logging.WARNING)

# Connect to AWS API
region = os.environ['AWS_REGION']
ownerid = os.environ['ownerid']
ec2 = boto3.resource("ec2", region_name=region)
cw = boto3.client("cloudwatch")
mode = 'null'

#set the date to today for the snapshot
today = datetime.now().date()

def delete_snap(stype, val, volid):
  print 'Deleting Old Snaps for ' + volid
  # Find old snaps for this volume
  snaps = ec2.snapshots.filter(Filters=[{'Name': 'owner-id', 'Values': [ownerid]}, {'Name': 'description', 'Values': [stype+'-'+volid+'-*']} ]).all()
  print snaps
  if stype == 'daily':
    days = int(val)
  if stype == 'monthly':
    days = int(val)*30
  for snap in snaps:
    print snap.id + ' ' + str(snap.start_time)
    remove_time = datetime.now() - timedelta(days=days)
    # Remove snap if older than days in meta
    if snap.start_time.date() <= remove_time.date():
      print 'Deleting Snap: ' + snap.id + ' For Volume: ' + volid
      snap.delete()
    else:
      print 'Retaining Snap: ' + snap.id

def take_snap(stype, val, volid):
  print 'Taking ' + stype + ' snap for volume: ' + volid
  if stype == 'monthly':
    description = 'monthly-' + volid + '-' + str(today)
    # Create snap
    snapshot = ec2.create_snapshot(VolumeId=volid, Description=description)
    # tag the snapshot
    snapshot.create_tags(Tags=[{'Key': 'Name', 'Value': description}])
    snapshot.create_tags(Tags=[{'Key': 'CreatedBy', 'Value': 'sls-snapmanage'}])
  elif stype == 'daily':
    description = 'daily-' + volid + '-' + str(today)
    # Create snap
    snapshot = ec2.create_snapshot(VolumeId=volid, Description=description)
    # tag the snapshot
    snapshot.create_tags(Tags=[{'Key': 'Name', 'Value': description}])
    snapshot.create_tags(Tags=[{'Key': 'CreatedBy', 'Value': 'sls-snapmanage'}])

def count_snaps(ownerid):
  daily_snaps = 0
  monthly_snaps = 0
  daily_snaps = ec2.snapshots.filter(Filters=[{'Name': 'owner-id', 'Values': [ownerid]}, {'Name': 'description', 'Values': ['daily-*']} ]).all()
  monthly_snaps = ec2.snapshots.filter(Filters=[{'Name': 'owner-id', 'Values': [ownerid]}, {'Name': 'description', 'Values': ['monthly-*']} ]).all()
  ds = sum(1 for x in daily_snaps)
  ms = sum(1 for x in monthly_snaps)
  return(ds, ms)

def report_cw_metrics(tv, mv, ds, ms):
  print 'total volumes: ' + str(tv)
  print 'managed volumes: ' + str(mv)
  print 'total daily snapshots: ' + str(ds)
  print 'total monthly snapshots: ' + str(ms)
  response_tv = cw.put_metric_data(
    Namespace='SnapManager',
    MetricData=[
        {
            'MetricName': 'Total',
            'Value': tv,
            'Dimensions': [
              {
                'Name': 'EBS',
                'Value': 'Volumes'
              },
            ],
            'Timestamp': str(datetime.now().date()),
            'Unit': 'Count'
        },
    ]
   )
  response_man = cw.put_metric_data(
    Namespace='SnapManager',
    MetricData=[
        {
            'MetricName': 'Managed',
            'Value': mv,
            'Dimensions': [
              {
                'Name': 'EBS',
                'Value': 'Volumes'
              },
            ],
            'Timestamp': str(datetime.now().date()),
            'Unit': 'Count'
        },
    ]
   )
  response_ds = cw.put_metric_data(
    Namespace='SnapManager',
    MetricData=[
        {
            'MetricName': 'Daily',
            'Value': ds,
            'Dimensions': [
              {
                'Name': 'EBS',
                'Value': 'Snapshots'
              },
            ],
            'Timestamp': str(datetime.now().date()),
            'Unit': 'Count'
        },
    ]
   )
  response_ms = cw.put_metric_data(
    Namespace='SnapManager',
    MetricData=[
        {
            'MetricName': 'Monthly',
            'Value': ms,
            'Dimensions': [
              {
                'Name': 'EBS',
                'Value': 'Snapshots'
              },
            ],
            'Timestamp': str(datetime.now().date()),
            'Unit': 'Count'
        },
    ]
   )

def readmeta(meta, volid):
  # get meta data and split it up
  meta_split = meta.split('|', 1)
  daily_val = meta_split[0]
  monthly_val = meta_split[1]
  if (daily_val != 0 and mode == 'daily'):
    take_snap('daily', daily_val, volid)
    delete_snap('daily', daily_val, volid)
  if (monthly_val != 0 and mode == 'monthly'):
    take_snap('monthly', monthly_val, volid)
   # delete_snap('monthly', monthly_val, volid)

def snap(event, context):
  global mode
  postmessage = event['body']
  message = postmessage.split(":")
  print message[1]
  #mode = event['mode']
  mode = message[1]
  mv = 0
  # read all the volumes
  volumes = ec2.volumes.all()
  for vol in volumes:
    # skip volumes with no meta data regarding tagging
    if vol.tags is None:
      print "Skipping Volume: " + vol.id
    else:
      # read all the tags
      for tag in vol.tags:
        # start the snap process if meta data present  
        if tag.values()[1] == 'MakeSnap':
          mv += 1
          readmeta(tag.values()[0], vol.id)
  tv = sum(1 for x in volumes)
  ds, ms = count_snaps(ownerid)
  report_cw_metrics(tv, mv, ds, ms)
  message = "Total managed Volumes: %s of %s with %s snaps for daily and %s monthly" % (mv, tv, ds, ms)
  return { 
    'message' : message
  } 
